import { GridLayout, GridItem } from 'vue-grid-layout'

export default async ({ Vue }) => {
  Vue.component('grid-layout', GridLayout)
  Vue.component('grid-item', GridItem)
}
