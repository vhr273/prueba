import VueApexCharts from 'vue-apexcharts';

export default async ({ Vue }) => {
  Vue.component('apex-chart', VueApexCharts);
}
